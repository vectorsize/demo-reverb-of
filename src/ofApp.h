#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxMaxim.h"

#include "Reverb/Reverb.hpp"

class ofApp : public ofBaseApp {

public:
  void setup() override;
  void update() override;
  void draw() override;
  void exit() override;

  void keyPressed(int key) override;
  void keyReleased(int key) override;
  void mouseMoved(int x, int y) override;
  void mouseDragged(int x, int y, int button) override;
  void mousePressed(int x, int y, int button) override;
  void mouseReleased(int x, int y, int button) override;
  void mouseEntered(int x, int y) override;
  void mouseExited(int x, int y) override;
  void windowResized(int w, int h) override;
  void dragEvent(ofDragInfo dragInfo) override;
  void gotMessage(ofMessage msg) override;

  void audioOut(ofSoundBuffer &output) override;
  ofSoundStream mSoundStream;

  void setPredelay(float &v);
  void setBandwidth(float &v);
  void setDamping(float &v);
  void setTime(float &v);
  void setMix(float &v);

  ofxPanel gui;
  ofParameter<float> mPredelay;
  ofParameter<float> mBandwidth;
  ofParameter<float> mDamping;
  ofParameter<float> mTime;
  ofParameter<float> mMix;

  maxiSample mSample;
  demo::Reverb mDemoRev;
};
