#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {

  int sampleRate = 44100;
  int bufferSize = 512;
  int numOutputChannels = 2;
  int numInputChannels = 0;

  // ofSound
  ofSoundStreamSettings settings;
  settings.sampleRate = sampleRate;
  settings.numOutputChannels = numOutputChannels;
  settings.numInputChannels = numInputChannels;
  settings.bufferSize = bufferSize;

  settings.setOutListener(this);
  mSoundStream.setup(settings);

  // ofMaxim
  ofxMaxiSettings::setup(sampleRate, numOutputChannels, bufferSize);

  //   GUI
  gui.setup("DemoVerb");
  // init with defaults
  gui.add(mPredelay.set("Predelay", mDemoRev.kPreDelay, 0.01f, 1.f));
  gui.add(mBandwidth.set("Bandwidth", mDemoRev.kBandwidth, 0.f, 1.f));
  gui.add(mDamping.set("Damping", mDemoRev.kDamping, 0.f, 1.f));
  gui.add(mTime.set("Time", mDemoRev.kTime, 0.f, 1.f));
  gui.add(mMix.set("Mix", mDemoRev.kDrywet, 0.f, 1.f));

  mPredelay.addListener(this, &ofApp::setPredelay);
  mBandwidth.addListener(this, &ofApp::setBandwidth);
  mDamping.addListener(this, &ofApp::setDamping);
  mTime.addListener(this, &ofApp::setTime);
  mMix.addListener(this, &ofApp::setMix);

  auto loopPath = ofToDataPath("loop.wav");
  mSample.load(loopPath);
  mDemoRev.setup();
}

//--------------------------------------------------------------
void ofApp::setPredelay(float &v) { mDemoRev.setPredelay(v); }
//--------------------------------------------------------------
void ofApp::setBandwidth(float &v) { mDemoRev.setBandwidth(v); }
//--------------------------------------------------------------
void ofApp::setDamping(float &v) { mDemoRev.setDamping(v); }
//--------------------------------------------------------------
void ofApp::setTime(float &v) { mDemoRev.setTime(v); }
//--------------------------------------------------------------
void ofApp::setMix(float &v) { mDemoRev.setMix(v); }

//--------------------------------------------------------------
void ofApp::update() {}

//--------------------------------------------------------------
void ofApp::draw() { gui.draw(); }

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer &output) {
  std::size_t outChannels = output.getNumChannels();
  for (int i = 0; i < output.getNumFrames(); ++i) {
    float sample = mSample.play();

    mDemoRev.processSamples(sample, sample);

    output[i * outChannels] = mDemoRev.outputL;
    output[i * outChannels + 1] = mDemoRev.outputR;
  }
}

//--------------------------------------------------------------
void ofApp::exit() {
  mPredelay.removeListener(this, &ofApp::setPredelay);
  mBandwidth.removeListener(this, &ofApp::setBandwidth);
  mDamping.removeListener(this, &ofApp::setDamping);
  mTime.removeListener(this, &ofApp::setTime);
  mMix.removeListener(this, &ofApp::setMix);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {}
