//
//  lfo.h
//
//  Created by Victor Saiz on 08/11/2020.
//

#ifndef lfo_h
#define lfo_h

#include "defines.h"
#include <math.h>

constexpr const uint32_t kTableLength = 4096;

class LFO {
public:
  LFO() { mInit = false; }
  void Init() { Init(1.0); }

  // precalculate sine and initialize
  void Init(const float freq) {
    mFrequency = freq;
    mStepSize = mFrequency * static_cast<float>(kTableLength) / mSampleRate;

    for (auto i = 0; i < kTableLength; ++i) {
      mSine[i] = sin(2.0 * M_PI * static_cast<float>(i) /
                     static_cast<float>(kTableLength));
    }

    mPhasor = 0.0;
    mInit = true;
  }

  float process() {
    if (!mInit)
      return 0;
    const float tableLength = static_cast<float>(kTableLength);

    mPlusPhase = mPhasor + phase * tableLength;

    // wrap mPlusPhase
    if (mPlusPhase < 0.0) {
      mPlusPhase += tableLength;
    } else if (mPlusPhase >= tableLength) {
      mPlusPhase -= tableLength;
    }

    // interpolate
    mA = static_cast<long>(mPlusPhase);
    mFrac = mPlusPhase - mA;
    mB = mA + 1.0f;
    mB %= kTableLength;

    output = mSine[mA] * (1.0f - mFrac) + mSine[mB] * mFrac;

    mPhasor += mStepSize;

    // wrap mPhasor
    if (mPhasor >= tableLength)
      mPhasor -= tableLength;

    return output;
  }

  // update freq and stepsize
  void setFrequency(const float frequency) {
    mFrequency = frequency;
    updateStepSize();
  }

  void setSamplerate(const float sampleRate) {
    mSampleRate = sampleRate;
    updateStepSize();
  }

private:
  float mFrequency;
  float mSampleRate = SAMPLE_RATE;
  float mStepSize;
  float mPhasor;
  float mPlusPhase;
  long mA, mB;
  float mFrac;
  float mSine[kTableLength];
  bool mInit = false;
  float output = 0.0;
  float phase = 0.0;

  void updateStepSize() {
    mStepSize = mFrequency * static_cast<float>(kTableLength) / SAMPLE_RATE;
  }
};

#endif /* lfo_h */
